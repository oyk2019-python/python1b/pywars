from karakter import Sith, Jedi

onur = Sith()
dusman = Jedi()

tur = 1

while True:
    print(tur, ". TUR")
    print("Filiz: ", onur.can)
    print("Dusman: ", dusman.can)
    print("""
1. Saldir
2. Iyiles
          """)
    hamle = int(input("Hamle: "))
    if hamle == 1:
        dusman.hasar(onur.atak())
        if not dusman.hayatta():
            print("Kazandiniz")
            break
    if hamle == 2:
        onur.iyiles()
        print("Iyilestiniz.", onur.can)

    onur.hasar(dusman.atak())
    if not onur.hayatta():
        print("Dusman Kazandi")
        break

    print("#~---~#")
    tur += 1
